package com.example.appmicalculadora

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private lateinit var txtUsuario : EditText
    private lateinit var txtContraseña : EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        this.iniciarComponentes()
        this.eventoClick()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    public fun iniciarComponentes() {
        this.txtUsuario = findViewById(R.id.txtUsuario)
        this.txtContraseña = findViewById(R.id.txtContraseña)
        this.btnSalir = findViewById(R.id.btnCerrar)
        this.btnIngresar = findViewById(R.id.btnIngresar)
    }

    public fun eventoClick() {
        this.btnIngresar.setOnClickListener(View.OnClickListener {
            var usuario : String = getString(R.string.usuario)
            var contrasena : String = getString(R.string.contraseña)
            var nombre : String = getString(R.string.nombre)

            if(txtUsuario.text.toString().contentEquals(usuario) && txtContraseña.text.toString().contentEquals(contrasena)) {
                var intent = Intent(this, OperacionesActivity::class.java)
                intent.putExtra("usuario", nombre)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Faltó información o corresponde al usuario", Toast.LENGTH_SHORT).show()
            }
        })

        this.btnSalir.setOnClickListener(View.OnClickListener {
          this.finish()
        })
    }
}