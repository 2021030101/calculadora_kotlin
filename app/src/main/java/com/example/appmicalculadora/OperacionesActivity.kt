package com.example.appmicalculadora

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class OperacionesActivity : AppCompatActivity() {
    private lateinit var txtUsuario: TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var txtResultado: TextView

    private lateinit var btnSumar: Button
    private lateinit var btnResta: Button
    private lateinit var btnMul: Button
    private lateinit var btnDiv: Button

    private lateinit var btnCerrar: Button
    private lateinit var btnLimpiar: Button
    private lateinit var operaciones: Operaciones

    var opcion : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_operaciones)

        iniciarComponentes()
        eventosClick()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    public fun iniciarComponentes() {
        this.txtUsuario = findViewById(R.id.txtUsuarion)
        this.txtResultado = findViewById(R.id.txtResultado)
        this.txtNum1 = findViewById(R.id.txtNum1)
        this.txtNum2 = findViewById(R.id.txtNum2)

        this.btnSumar = findViewById(R.id.btnSumar)
        this.btnResta = findViewById(R.id.btnRestar)
        this.btnMul = findViewById(R.id.btnMultiplicar)
        this.btnDiv = findViewById(R.id.btnDividir)

        this.btnLimpiar = findViewById(R.id.btnLimpiar)
        this.btnCerrar = findViewById(R.id.btnRegresar)

        val bundle : Bundle? = intent.extras
        txtUsuario.text = bundle?.getString("usuario")
    }

    public fun validar(): Boolean {
        if(txtNum1.text.toString().contentEquals("") || txtNum2.text.toString().contentEquals(""))
            return false
        return true
    }

    public fun operaciones(): Float {
        var num1 : Float = 0f
        var num2 : Float = 0f
        var res : Float = 0f

        if(validar()) {
            num1 = txtNum1.text.toString().toFloat()
            num2 = txtNum2.text.toString().toFloat()
            operaciones = Operaciones(num1, num2)
            when(opcion) {
                1 -> { res=operaciones.sumar() }
                2 -> { res=operaciones.restar() }
                3 -> { res=operaciones.multiplicar() }
                4 -> { res=operaciones.dividir() }
            }
        } else Toast.makeText(this, "Faltaron datos por capturar", Toast.LENGTH_SHORT).show()
        return res
    }

    public fun eventosClick() {
        btnSumar.setOnClickListener(View.OnClickListener {
            opcion = 1
            txtResultado.text = operaciones().toString()
        })

        btnResta.setOnClickListener(View.OnClickListener {
            opcion = 2
            txtResultado.text = operaciones().toString()
        })

        btnMul.setOnClickListener(View.OnClickListener {
            opcion = 3
            txtResultado.text = operaciones().toString()
        })

        btnDiv.setOnClickListener(View.OnClickListener {
            if(this.txtNum2.text.toString().toFloat() == 0f) {
                txtResultado.text = "No es posible división sobre 0"
                return@OnClickListener
            }
            opcion = 4
            txtResultado.text = operaciones().toString()
        })

        btnLimpiar.setOnClickListener(View.OnClickListener {
            this.txtResultado.text = "Resultado: "
            this.txtNum1.text.clear()
            this.txtNum2.text.clear()
        })

        btnCerrar.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage("¿Deseas cerrar la app?")
            builder.setPositiveButton(android.R.string.yes) {
                dialog, whitch -> this.finish()
            }
            builder.setNegativeButton(android.R.string.no) {
                dialog, whitch -> this.finish()
            }
            builder.show()
        })
    }
}